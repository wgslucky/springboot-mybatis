package com.xinyue.myapp.service;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xinyue.myapp.dao.mapper.AccountEntityMapper;
import com.xinyue.myapp.dao.model.AccountEntity;

@Service
public class AccountService {
    
    @Autowired
    private AccountEntityMapper accountEntityMapper;
    @PostConstruct
    public void test() {
      AccountEntity accountEntity =  accountEntityMapper.selectByPrimaryKey(12L);
      System.out.println("查询成功：" + accountEntity.getCreatetime());
    }
}
