package com.xinyue.myapp.dao.mapper;

import com.xinyue.myapp.dao.model.AccountEntity;

public interface AccountEntityMapper {
    int deleteByPrimaryKey(Long userid);

    int insert(AccountEntity record);

    int insertSelective(AccountEntity record);

    AccountEntity selectByPrimaryKey(Long userid);

    int updateByPrimaryKeySelective(AccountEntity record);

    int updateByPrimaryKey(AccountEntity record);
}